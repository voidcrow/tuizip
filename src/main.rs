// TuiZIP  Copyright (C) 2024  Void Crow <voidcrow@systemli.org>
// This file is part of TuiZIP and licensed under the conditions of the GNU Public License 3.0.
// The file LICENSE in the projects root directory contains more information.

use anyhow::{anyhow, bail, Context, Result};
use clap::Parser;
use crossterm::event::Event::Key;
use crossterm::event::{KeyEvent, KeyModifiers};
use crossterm::{
    event::{self, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand,
};
use ratatui::{prelude::*, widgets::*};
use std::collections::HashMap;
use std::ffi::OsString;
use std::fs::File;
use std::io::{stdout, Read, Seek};
use std::path::{Path, PathBuf};
use strum::Display;
use zip::ZipArchive;

#[derive(Debug, Parser)]
struct Args {
    /// The path to the archive to open
    archive: String,
}

pub trait View {
    fn draw(&self, frame: &mut Frame, state: &State);
    fn close(&self, state: &mut State);
}

#[derive(Debug, Clone)]
struct Entry {
    entry_type: EntryType,
    index: usize,
}

#[derive(Debug, Clone, Display)]
enum EntryType {
    File,
    Directory(HashMap<OsString, Entry>),
}

#[derive(Debug)]
struct Tree {
    root: HashMap<OsString, Entry>,
}

impl<'a> Tree {
    pub fn new(archive: &mut ZipArchive<impl Read + Seek>) -> Result<Self> {
        let mut tree = Self {
            root: HashMap::new(),
        };

        let entries = archive
            .file_names()
            .map(|name| name.into())
            .collect::<Vec<PathBuf>>();

        for entry in entries.iter() {
            let mut vfs_parent = &mut tree.root;
            match entry.parent() {
                None => (),
                Some(path) => {
                    for comp in path {
                        let comp = comp.to_owned();
                        if !vfs_parent.contains_key(&comp) {
                            vfs_parent.insert(
                                comp.clone(),
                                Entry {
                                    index: 0,
                                    entry_type: EntryType::Directory(HashMap::new()),
                                },
                            );
                        }
                        vfs_parent = match &mut vfs_parent
                            .get_mut(&comp)
                            .context("failed to lookup a component")?
                            .entry_type
                        {
                            EntryType::File => bail!("tried to threat a file like a directory"),
                            EntryType::Directory(map) => map,
                        };
                    }
                }
            }
            match entry.file_name() {
                Some(name) => {
                    vfs_parent.insert(
                        name.to_owned(),
                        Entry {
                            index: 0,
                            entry_type: if archive
                                .by_name(&entry.as_os_str().to_string_lossy())?
                                .is_file()
                            {
                                EntryType::File
                            } else {
                                EntryType::Directory(HashMap::new())
                            },
                        },
                    );
                }
                None => (),
            };
        }

        Ok(tree)
    }

    pub fn content(
        &'a self,
        path: &Path,
    ) -> Result<(
        Vec<(&'a OsString, &'a Entry)>,
        Vec<(&'a OsString, &'a Entry)>,
    )> {
        let mut vfs_parent = &self.root;
        for comp in path {
            let comp = comp.to_owned();
            vfs_parent = match &vfs_parent
                .get(&comp)
                .context("failed to lookup a component")?
                .entry_type
            {
                EntryType::File => bail!("tried to threat a file like a directory"),
                EntryType::Directory(map) => map,
            };
        }
        Ok(vfs_parent.iter().partition(|&(_, e)| match e.entry_type {
            EntryType::Directory(_) => true,
            EntryType::File => false,
        }))
    }
}

struct State {
    running: bool,

    archive: String,
    path: PathBuf,

    tree: Tree,
    list: Vec<(OsString, Entry)>,

    views: Vec<Box<dyn View>>,
    list_state: ListState,
}

impl State {
    fn update(&mut self) -> Result<()> {
        let (dirs, files) = self.tree.content(&self.path)?;
        self.list = dirs
            .iter()
            .map(|&(name, entry)| (name.to_owned(), entry.to_owned()))
            .collect();

        self.list.extend(
            files
                .iter()
                .map(|&(name, entry)| (name.to_owned(), entry.to_owned()))
                .collect::<Vec<(OsString, Entry)>>(),
        );
        self.list_state.select(None);
        Ok(())
    }

    fn go_deeper(&mut self) -> Result<()> {
        let selected = match self.list_state.selected() {
            Some(s) => s,
            None => return Err(anyhow!("Can't go deeper with no entry selected!")),
        };
        let entry = &self.list[selected];
        match &entry.1.entry_type {
            EntryType::Directory(..) => (),
            t => return Err(anyhow!("Can't go deeper on an entry of type {t}.")),
        }

        self.path.push(entry.0.clone());
        cli_log::debug!("Path updated: {:?}", self.path);

        self.update()?;
        Ok(())
    }

    fn go_shallower(&mut self) -> Result<()> {
        if !self.path.pop() {
            return Err(anyhow!("Can't go shallower than root"));
        }

        self.update()?;
        Ok(())
    }
}

fn main() -> Result<()> {
    cli_log::init_cli_log!();
    let args = Args::parse();

    let mut archive = ZipArchive::new(File::open(&args.archive)?)?;

    enable_raw_mode()?;
    stdout().execute(EnterAlternateScreen)?;
    let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
    let mut state = State {
        running: true,
        archive: args.archive,
        path: std::path::PathBuf::new(),
        tree: Tree::new(&mut archive)?,
        list: Vec::new(),
        views: Vec::new(),
        list_state: ListState::default(),
    };

    state.update()?;

    while state.running {
        terminal.draw(|f| draw(&mut state, f).unwrap())?;
        handle_event(&mut state)?;
    }

    disable_raw_mode()?;
    stdout().execute(LeaveAlternateScreen)?;
    Ok(())
}

fn handle_event(state: &mut State) -> Result<()> {
    if event::poll(std::time::Duration::from_millis(50))? {
        if let Key(key) = event::read()? {
            match key {
                KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Char('q'),
                    ..
                }
                | KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Char('c'),
                    modifiers: KeyModifiers::CONTROL,
                    ..
                } => {
                    state.running = false;
                }
                KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Esc,
                    ..
                } => match state.views.pop() {
                    Some(view) => {
                        view.close(state);
                    }
                    None => {
                        state.running = false;
                    }
                },
                KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Down,
                    ..
                } => state.list_state.select(match state.list_state.selected() {
                    Some(index) => Some(if index + 1 < state.list.len() {
                        index + 1
                    } else {
                        index
                    }),
                    None => Some(0),
                }),

                KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Up,
                    ..
                } => state.list_state.select(match state.list_state.selected() {
                    Some(index) => {
                        if index > 0 {
                            Some(index - 1)
                        } else {
                            None
                        }
                    }
                    None => None,
                }),

                KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Enter,
                    ..
                } => {
                    let _ = state.go_deeper();
                }

                KeyEvent {
                    kind: event::KeyEventKind::Press,
                    code: KeyCode::Backspace,
                    ..
                } => {
                    let _ = state.go_shallower();
                }

                _ => (),
            }
        }
    }
    Ok(())
}

fn draw(state: &mut State, frame: &mut Frame) -> Result<()> {
    let chunks = Layout::new(
        Direction::Vertical,
        [
            Constraint::Length(3),
            Constraint::Min(4),
            Constraint::Length(3),
        ],
    )
    .split(frame.size());
    let body_chunks = Layout::new(
        Direction::Horizontal,
        [Constraint::Min(32), Constraint::Length(32)],
    )
    .split(chunks[1]);

    let header = Paragraph::new(concat!("tuiZIP v", env!("CARGO_PKG_VERSION"))).block(
        Block::default()
            .borders(Borders::ALL)
            .style(Style::default().fg(Color::Magenta)),
    );

    let body = List::new(
        state
            .list
            .iter()
            .map(|(name, e)| {
                let name = match e.entry_type {
                    EntryType::Directory(..) => format!("{}/", name.to_string_lossy()),
                    _ => name.to_string_lossy().to_string(),
                };

                ListItem::new(name).style(Style::default().fg(match e.entry_type {
                    EntryType::Directory(_) => Color::Green,
                    EntryType::File => Color::Cyan,
                }))
            })
            .collect::<Vec<ListItem>>(),
    )
    .highlight_symbol("> ")
    .highlight_style(Style::default().fg(Color::Yellow))
    .block(Block::default().borders(Borders::ALL))
    .style(Style::default().fg(Color::Magenta));

    let info = Paragraph::new(match state.list_state.selected() {
        None => Text::from(vec![Line::styled("Archive Info", Style::default())]),
        Some(index) => {
            let e = &state.list[index].1;
            match e.entry_type {
                EntryType::Directory(_) => {
                    Text::from(vec![Line::styled("Directory Info", Style::default())])
                }
                EntryType::File => Text::from(vec![Line::styled("File Info", Style::default())]),
            }
        }
    })
    .block(
        Block::default()
            .borders(Borders::ALL)
            .style(Style::default().fg(Color::Magenta)),
    );

    let footer = Paragraph::new(format!(
        "{} > /{}",
        state.archive,
        state.path.to_string_lossy()
    ))
    .block(
        Block::default()
            .borders(Borders::ALL)
            .style(Style::default().fg(Color::Magenta)),
    );

    frame.render_widget(header, chunks[0]);
    frame.render_stateful_widget(body, body_chunks[0], &mut state.list_state);
    frame.render_widget(info, body_chunks[1]);
    frame.render_widget(footer, chunks[2]);

    for view in state.views.iter() {
        view.draw(frame, state);
    }
    Ok(())
}
